#include "common.hpp"

class complejo_c{
  
private:
  
  float real;
  float imag;
  
public:
  
  //Constructores  
  complejo_c (float = 2, float = 2);
  complejo_c (const complejo_c&);
  
  //Destructor
//   ~complejo_c (void);
  
  //Acceso a atributos
  float get_real (void) const;
  float get_imag (void) const;
  void set_c (float, float);
  void set_real (float);
  void set_imag (float);
  
  //Sobrecarga de operadores aritméticos
  const complejo_c operator+ (const complejo_c&);
  const complejo_c operator- (const complejo_c&);
  const complejo_c operator* (const complejo_c&);
  const complejo_c operator/ (complejo_c&);
  
  //Sobrecarga de operador asignación
  complejo_c& operator= (const complejo_c&);  
  
  //Sobrecarga de operadores de flujo
  friend ostream& operator<< (ostream&, const complejo_c&);
  friend istream& operator>> (istream&, complejo_c&);

};