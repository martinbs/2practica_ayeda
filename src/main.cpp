#include "common.hpp"
#include "calculadora.hpp"
#include "racional.hpp"
#include "complejo.hpp"

int main (void){
  
  string stripe (50, '-');
  int opcion = 0;  
  
  while (opcion != 5){        
    cout << "Calculadora de expresiones en notación postfija" << endl
    << right << stripe << endl
    << "1. Enteros" << endl
    << "2. Flotantes" << endl
    << "3. Racionales" << endl
    << "4. Complejos" << endl
    << "5. Salir" << endl
    << "Introducir opcion: ";
    
    cin >> opcion;
    system ("clear");
    cin.ignore();
    
    switch(opcion){
      case 1:
	calculadora <int> (cin);
	break;
      
      case 2:
	calculadora <float> (cin);
	break;

      case 3:
	calculadora <racional_c> (cin);
	break;

      case 4:
	calculadora <complejo_c> (cin);
	break;

      case 5:
	return 0;
	break;

      default:
	break;
    }    
  }
}


