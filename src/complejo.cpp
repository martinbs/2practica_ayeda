#include "complejo.hpp"
//
//Constructores
complejo_c::complejo_c (float re, float im): real(re), imag(im){
  
} 

complejo_c::complejo_c (const complejo_c& complejo){
  (*this) = complejo;
}

//
//Destructor
// complejo_c::~complejo_c (void){}

//
//Acceso a atributos
float complejo_c::get_real (void) const{
  return real;
}

float complejo_c::get_imag (void) const{
  return imag;
}

void complejo_c::set_c (float re, float im){
  real = re;
  imag = im;
}

void complejo_c::set_real (float re){
  real = re;
}

void complejo_c::set_imag (float im){
  imag = im;
}

//
//Sobrecarga de operadores aritméticos
const complejo_c complejo_c::operator+ (const complejo_c& comp){
  complejo_c resultado (real+comp.get_real(), imag+comp.get_imag());
  
  return resultado;
}

const complejo_c complejo_c::operator- (const complejo_c& comp){
  complejo_c resultado (real-comp.get_real(), imag-comp.get_imag());
  
  return resultado;
}

//(a + bi) · (c + di) = (ac − bd) + (ad + bc)i
const complejo_c complejo_c::operator* (const complejo_c& comp){
  float a, b, c, d;
  a = real;
  b = imag;
  c = comp.get_real();
  d = comp.get_imag();
  
  complejo_c resultado ((a*c - b*d), (a*d + b*c));
  
  return resultado;
}

//(a+bi)/(c+di) = (ac + bd)/(c²+d²) + [(bc - ad)/(c²+d²)]i
const complejo_c complejo_c::operator/ (complejo_c& comp){
  float a, b, c, d;
  a = real;
  b = imag;
  c = comp.get_real();
  d = comp.get_imag();
  
  complejo_c resultado ( ((a*c + b*d)/(c*c + d*d)),
			 ((b*c - a*d)/(c*c + d*d))
  );
  
  return resultado;
}

//
//Sobrecarga de operador asignación
complejo_c& complejo_c::operator= (const complejo_c& comp){
  set_c(real, imag);
  return (*this);
}

//
//Sobrecarga de operadores de flujo
ostream& operator<< (ostream& os, const complejo_c& comp){
  if (comp.get_real() != 0) 
    os << comp.get_real();
    
  if (comp.get_imag() < 0)
    os << comp.get_imag() << 'i';
  else if (comp.get_imag() != 0)
    os << '+' << comp.get_imag() << 'i';  
  
  return os;
}

istream& operator>> (istream& is, complejo_c& comp){
  string s;
  is >> s;
  
  size_t div = s.find("+");
  size_t div2 = s.find("-");
  
  comp.set_real ( atoi( s.substr(0, div).c_str() ) );
  if (div != string::npos)
    comp.set_imag( atoi(s.substr(div+1, string::npos-1).c_str() ) );
  else if (div2 != string::npos)
    comp.set_imag( atoi(s.substr(div2+1, string::npos-1).c_str() )*-1 );
  else
    comp.set_imag(1);
  
  return is;
}